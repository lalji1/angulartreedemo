import {Component, Inject, Input, Renderer2, ViewChild, ElementRef,OnInit, AfterViewInit, ViewEncapsulation} from '@angular/core';
import { Browser, closest, createElement } from '@syncfusion/ej2-base';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {sampleData} from  './jsontreegriddata';
import {DataManager, WebApiAdaptor} from '@syncfusion/ej2-data';
import { DialogComponent, ButtonPropsModel  } from '@syncfusion/ej2-angular-popups';
import { DropDownListComponent } from '@syncfusion/ej2-angular-dropdowns';
import { createCheckBox } from '@syncfusion/ej2-buttons';
import {GridComponent, ContextMenuItemModel, DialogEditEventArgs, SaveEventArgs, InfiniteScrollService} from '@syncfusion/ej2-angular-grids';
import {
    TreeGridComponent,
    EditService,
    EditSettingsModel,
    ToolbarService,
    PageService,
    SortService,
    ResizeService,
    ContextMenuService,
    ColumnChooserService,
    RowDDService,
    SelectionService,
    FreezeService,
    ColumnMenuService,
    EditSettings,
    VirtualScrollService,
} from '@syncfusion/ej2-angular-treegrid';
import {EmitType, getValue, isNullOrUndefined} from '@syncfusion/ej2-base';
import {BeforeOpenCloseEventArgs} from '@syncfusion/ej2-inputs';
import {MenuEventArgs, ContextMenu, ContextMenuModel, BeforeOpenCloseMenuEventArgs} from '@syncfusion/ej2-navigations';
import {parentsUntil} from '@syncfusion/ej2-grids';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    providers: [ToolbarService, EditService, PageService, SortService, ResizeService, ContextMenuService, ColumnChooserService, RowDDService, SelectionService, FreezeService, ColumnMenuService, EditSettings, VirtualScrollService, InfiniteScrollService],
    styleUrls: ['./app.component.css'],
    encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit, AfterViewInit  {
    public data: Object[] = [];
    public columnsData?: any;
    @ViewChild('treegrid')
    public treegrid: TreeGridComponent;
    @ViewChild ('columngrid') private columngrid: ElementRef<HTMLElement>;
    @ViewChild('headercontextmenu')
    public headercontextmenu: ContextMenu;
    @ViewChild('addEditColumn')
    public template: DialogComponent;
    @ViewChild('columnType')
    public listObj: DropDownListComponent;
    @ViewChild('columnAlignment')
    public alignmentObj: DropDownListComponent;
    //@ViewChild('editcol') editcol: ElementRef;
    @ViewChild('editcol',{static:false, read: ElementRef})  editcol: ElementRef;
    public columnMode: string;
    @ViewChild('DeleteDialog')
    public DeleteDialog: DialogComponent;
    //public dlgButtons: ButtonPropsModel[] = [{ click: this.confirmDlgBtnClick.bind(this), buttonModel: { content: 'Yes', isPrimary: true } }, { click: this.confirmDlgBtnClick.bind(this), buttonModel: { content: 'No' } }];
    public animationSettings: Object = { effect: 'None' };
    public showCloseIcon: boolean = true;
    public target?: string = '.control-section';
    public hidden: Boolean = false;
    public columnForm: FormGroup = new FormGroup({
        columnname: new FormControl(''),
        columntype: new FormControl(''),
        columndefaultvalue: new FormControl(''),
        columnwidth: new FormControl(''),
        columnfontsize: new FormControl(''),
        columnfontcolor: new FormControl(''),
        columnfontbgcolor: new FormControl(''),
        columnalignment: new FormControl(''),
        columntextwrap: new FormControl(false),
    });
    public editSettings?: Object;
    public toolbar?: string[];
    public filterSettings: Object;
    public pageSettings?: Object;
    public contextMenuItems?: Object;
    public headermenuItems?: Object;
    public numericParams?: Object;
    public dpParams?: Object;
    public taskidrules?: Object;
    public tasknamerules?: Object;
    public startdaterules?: Object;
    public priorityrules?: Object;
    public durationrules?: Object;
    public progressrules?: Object;
    public format?: Object;
    public selectOptions?: Object;
    public rowPosition?: string;
    public columnDataType?: any;
    public rowIndex?: number;
    public cellIndex?: number;
    public domResult?: any;
    public selectedEditColumn: string;
    public dialogVisibility: boolean = false;
    public dataTypeData?: Object[] = [
        { Id: 'string', Type: 'String' },
        { Id: 'number', Type: 'Number' },
        { Id: 'boolean', Type: 'Boolean' },
        { Id: 'date', Type: 'Date' },
        { Id: 'datetime', Type: 'DateTime' }
    ];
    public value?: string = '';
    public fields?: Object = { text: 'Type', value: 'Id' };

    public alignmentTypeData?: Object[] = [
        { Id: 'Left', Type: 'Left' },
        { Id: 'Right', Type: 'Right' },
        { Id: 'Center', Type: 'Center' }
    ];
    public alignmentValue?: string = '';
    public alignmentFields?: Object = { text: 'Type', value: 'Id' };
    public isDeskTop?: Boolean;
    public isEditCol?: any;
    constructor(public formBuilder: FormBuilder, private elementRef:ElementRef,  private renderer:Renderer2) {
        this.createForm();
        this.editSettings = {
            allowEditing: true,
            allowAdding: true,
            allowDeleting: true,
            mode: "Dialog",
            showDeleteConfirmDialog:true,
            newRowPosition: this.rowPosition
        };
        console.log(elementRef.nativeElement);
    }

    ngOnInit() {
        this.data = sampleData;
        this.numericParams = {params: {format: 'n'}};
        this.format = {format: 'M/d/y hh:mm a', type: 'dateTime'};
        this.columnsData = [
            { field: "taskID", headerText: "Task ID", width: "70", textAlign:"Left", editType: "", format: '', validationRules: this.taskidrules, isPrimaryKey:'true' },
            { field: "taskName", headerText: "Task Name", width: "90", textAlign:"Left", editType: 'stringedit', format: '', validationRules: this.tasknamerules },
            { field: "startDate", headerText: "Start Date", width: "90", textAlign:"Left", editType:'datetimepickeredit', format: this.format, validationRules: this.startdaterules, edit: this.dpParams },
            { field: "approved", headerText: "Approved", width: "90", textAlign:"Left", editType: 'booleanedit', format: '', displayAsCheckBox:'true', validationRules: '' },
            { field: "progress", headerText: "Progress", width: "90", textAlign:"Left", editType: 'numericedit', format: '', validationRules: this.progressrules },
            { field: "priority", headerText: "Priority", width: "90", textAlign:"Left", editType: 'dropdownedit', format: '', validationRules: this.priorityrules },
        ];

        this.isDeskTop = !Browser.isDevice;
        this.toolbar = [];
        this.filterSettings = { type: 'FilterBar', hierarchyMode: 'Parent', mode: 'Immediate' };
        // this.contextMenuItems = ['AutoFit', 'AutoFitAll', 'SortAscending', 'SortDescending',
        //   'Edit', 'Delete', 'Save', 'Cancel',
        //   'PdfExport', 'ExcelExport', 'CsvExport', 'FirstPage', 'PrevPage',
        //   'LastPage', 'NextPage'];
        this.pageSettings = {pageCount: 5};

        this.dpParams = {params: {format: 'M/d/y hh:mm a'}};
        this.taskidrules = {required: true, number: true};
        this.tasknamerules = {required: true};
        this.startdaterules = {date: true};
        this.priorityrules = {required: true};
        this.progressrules = {number: true, min: 0};
        this.contextMenuItems = [
            {text: 'AddNext', target: '.e-content', id: 'addnext'},
            {text: 'AddChild', target: '.e-content', id: 'addchild'},
            {text: 'DelRow', target: '.e-content', id: 'delete'},
            {text: 'EditRow', target: '.e-content', id: 'editrow'},
            {text: 'MultiSelect', target: '.e-content', id: 'multiselect'},
            {text: 'CopyRows', target: '.e-content', id: 'copyrows'},
            {text: 'CutRows', target: '.e-content', id: 'cutrows'},
            {text: 'PasteNext', target: '.e-content', id: 'pastenext'},
            {text: 'PasteChild', target: '.e-content', id: 'pastchild'}
        ];
        this.headermenuItems = [
            {text: 'EditCol', id: 'editcol'},
            {text: 'NewCol', id: 'newcol'},
            {text: 'DelCol', id: 'delcol'},
            {text: 'ChooseCol', id: 'choosecol'},
            {text: 'FreezeCol', id: 'freezecol'},
            {text: 'FilterCol', id: 'filtercol'},
            {text: 'MultiSort', id: 'multisort'},
        ];
        this.selectOptions = {type: 'Multiple'};
        this.isEditCol = localStorage.getItem('editcol') == "true" ? true : false;
        // this.renderer.listen(this.elementRef.nativeElement.querySelector('#editcol'), 'change', (evt) => {
        //     console.log('Clicking the document', evt);
        // })
        //if(this.editcol.nativeElement !== undefined) {
        //}
        // let el = this.elementRef.nativeElement;
        // console.log(el);
    }
    ngAfterViewInit() {
        console.log(this.editcol);
        // this.editcol.nativeElement.innerHTML = "some html";
        // setTimeout(() => {
        //     console.log(this.editcol.nativeElement.innerText);
        // }, 1000);
       // if(this.elementRef.nativeElement !== undefined){
       //  this.renderer.listen(this.editcol.nativeElement, 'change', (event) => {
       //      alert('okk');
       //      //this.checkFunc();
       //  });
       // }
    }
    onLoad() {
        this.treegrid.grid.adaptiveDlgTarget = document.getElementsByClassName('e-mobile-content')[0] as HTMLElement;
    }
    createForm() : void {
        this.columnForm = this.formBuilder.group({
            columnname: ['', Validators.required],
            columntype: ['', Validators.required],
            columndefaultvalue: [''],
            columnwidth: ['', Validators.required],
            columnfontsize: ['', Validators.required],
            columnfontcolor: [''],
            columnfontbgcolor: [''],
            columnalignment: [''],
            columntextwrap: [false],
        });
    }
    onChangeDataType(args: any) {
        this.columnDataType = this.listObj.value.toString();
       // console.log(this.listObj.value.toString());
       console.log(this.columnDataType);
    }
    onChangeAlignment(args: any) {
        //console.log(this.alignmentObj.value.toString());
        console.log(this.alignmentObj.text);
    }
    // toggleHelp(input: any , e:MouseEvent) : void{
    //     input.helpOpen=!input.helpOpen;
    //
    //     console.log('event element',e);
    //     if ((e.target as HTMLElement).tagName === 'input') {
    //         let target = e.target as HTMLElement;
    //         alert(target.innerHTML)
    //     }
    // }
    checkVisible(data: any) {
        console.log(data);
       if(data.target.type === "checkbox") {
           let val = data.target.checked;
           localStorage.setItem('editcol', JSON.stringify(val));
           alert('yes');
       }

    }
    checkFunc(id: any) {
        console.log('ok');
    }

    onItemClick(data: any) {
        if(data.target.tagName=="span"){
            alert('SPAN CLICKED');
        }
    }
    itemRender(args: MenuEventArgs ) {
        let name1:string = args.item.text || '';
        let check: Element = createCheckBox(createElement, false, {
            label: args.item.text,
            checked: (args.item.text === name1 && localStorage.getItem(name1) === "true") ? true : false
        });
        args.element.innerHTML = '';
        args.element.appendChild(check);
        // let editCol = localStorage.getItem('editcol') == "true" ? true : false;
        // if (args.item.id === "editcol") {
        //     if(editCol) {
        //         args.element.innerHTML = `<input #editcol  type="checkbox" value="false" (change)="checkFunc()"/>&nbsp;<span class="e-disabled">EditCol</span>`;
        //     } else {
        //         args.element.innerHTML = `<input #editcol  type="checkbox" value="false" />&nbsp;<span>EditCol</span>`;
        //     }
        //     //let controlCheckbox = (<HTMLInputElement>document.getElementById("editcol"));
        //     //controlCheckbox.addEventListener("change", this.checkVisible.bind(args.item.id));
        //     //args.element.addEventListener("change", this.checkVisible.bind(args.item.id), false);
        // }
        // if (args.item.id === "newcol") {
        //     args.element.innerHTML = `<input id='test' type="checkbox"/><span>NewCol</span>`;
        // }
        // if (args.item.id === "delcol") {
        //     args.element.innerHTML = `<input type="checkbox"/><span>DelCol</span>`;
        // }
        // if (args.item.id === "choosecol") {
        //     args.element.innerHTML = `<input type="checkbox"/><span>ChooseCol</span>`;
        // }
        // if (args.item.id === "freezecol") {
        //     args.element.innerHTML = `<input type="checkbox"/><span>FreezeCol</span>`;
        // }
        // if (args.item.id === "filtercol") {
        //     args.element.innerHTML = `<input type="checkbox"/><span>FilterCol</span>`;
        // }
        // if (args.item.id === "multisort") {
        //     args.element.innerHTML = `<input type="checkbox"/><span>MultiSort</span>`;
        // }
    }
    beforeHeaderMenuClose(args:BeforeOpenCloseMenuEventArgs) {
        console.log(args);
        if ((args.event.target as Element).closest('.e-frame')) {
             args.cancel = true;
             //this.template.hide();
            let selectedElem: NodeList = args.element.querySelectorAll('.e-selected');
            for(let i:number=0; i < selectedElem.length; i++){
                let ele: Element = selectedElem[i] as Element;
                ele.classList.remove('e-selected');
            }
            let checkbox: HTMLElement = closest(args.event.target as Element, '.e-checkbox-wrapper') as HTMLElement;
            let frame: HTMLElement = checkbox.querySelector('.e-frame') as HTMLElement;
            let label: HTMLElement = checkbox.querySelector('.e-label') as HTMLElement;
            let key: string = label.innerText.toString();
            if (checkbox && frame.classList.contains('e-check')) {
                frame.classList.remove('e-check');
                let val = false;
                localStorage.setItem(key, JSON.stringify(val));
                args.cancel = true;
            } else if (checkbox) {
                frame.classList.add('e-check');
                let val = true;
                localStorage.setItem(key, JSON.stringify(val));
                args.cancel = true;
            }
            //frame.addEventListener('click', () => {alert('show'); args.cancel = true; this.template.hide(); });
            //label.addEventListener('click', () => {alert('hide'); args.cancel = false; this.template.show(); });
        }
        if ((args.event.target as Element).closest('.e-label')) {
            this.template.show()
        }
    }
    itemRowRender(args: MenuEventArgs) {
        let name1:string = args.item.text || '';
        let check: Element = createCheckBox(createElement, false, {
            label: args.item.text,
            checked: (args.item.text === name1 && localStorage.getItem(name1) === "true") ? true : false
        });
        args.element.innerHTML = '';
        args.element.appendChild(check);
        // if (args.item.id === 'addnext') {
        //     args.element.innerHTML = `<input id="addnext" type="checkbox" value="false"/>&nbsp;<span>AddNext</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'addchild') {
        //     args.element.innerHTML = `<input id="addchild" type="checkbox" value="false"/>&nbsp;<span>AddChild</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'delete') {
        //     args.element.innerHTML = `<input id="delete" type="checkbox" value="false"/>&nbsp;<span>DelRow</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'editrow') {
        //     args.element.innerHTML = `<input id="editrow" type="checkbox" value="false"/>&nbsp;<span>EditRow</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'multiselect') {
        //     args.element.innerHTML = `<input id="multiselect" type="checkbox" value="false"/>&nbsp;<span>MultiSelect</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'copyrows') {
        //     args.element.innerHTML = `<input id="copyrows" type="checkbox" value="false"/>&nbsp;<span>CopyRows</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'cutrows') {
        //     args.element.innerHTML = `<input id="cutrows" type="checkbox" value="false"/>&nbsp;<span>CutRows</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'pastenext') {
        //     args.element.innerHTML = `<input id="pastenext" type="checkbox" value="false"/>&nbsp;<span>PasteNext</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // } else if (args.item.id === 'pastchild') {
        //     args.element.innerHTML = `<input id="pastchild" type="checkbox" value="false"/>&nbsp;<span>PastChild</span>`;
        //     args.element.addEventListener("change", this.checkRowMenu.bind(args.item.id));
        // }
    }
    beforeRowMenuClose(args:BeforeOpenCloseMenuEventArgs ) {
        console.log(args);
        if ((args.event.target as Element).closest('.e-frame')) {
            args.cancel = true;
            //this.template.hide();
            let selectedElem: NodeList = args.element.querySelectorAll('.e-selected');
            for(let i:number=0; i < selectedElem.length; i++){
                let ele: Element = selectedElem[i] as Element;
                ele.classList.remove('e-selected');
            }
            let checkbox: HTMLElement = closest(args.event.target as Element, '.e-checkbox-wrapper') as HTMLElement;
            let frame: HTMLElement = checkbox.querySelector('.e-frame') as HTMLElement;
            let label: HTMLElement = checkbox.querySelector('.e-label') as HTMLElement;
            let key: string = label.innerText.toString();
            if (checkbox && frame.classList.contains('e-check')) {
                frame.classList.remove('e-check');
                let val = false;
                localStorage.setItem(key, JSON.stringify(val));
                args.cancel = true;
            } else if (checkbox) {
                frame.classList.add('e-check');
                let val = true;
                localStorage.setItem(key, JSON.stringify(val));
                args.cancel = true;
            }
            //frame.addEventListener('click', () => {alert('show'); args.cancel = true; this.template.hide(); });
            //label.addEventListener('click', () => {alert('hide'); args.cancel = false; this.template.show(); });
        }
        if ((args.event.target as Element).closest('.e-label')) {
            this.template.show()
        }
    }
    checkRowMenu(data: any) {
        if(data.target.type === "checkbox") {
            alert('yes');
        }
    }
    Submit() {
        if (this.columnMode === 'Add') {
            console.log(this.columnForm);
            let customStyle: any = {
                 color: this.columnForm.value.columnfontcolor,
                'background-color': this.columnForm.value.columnfontbgcolor
            };
            let newcolumnName = this.columnForm.value.columnname;
            sampleData.map((item: any) => {
                if (item.subtasks.length > 0 ) {
                    item.subtasks.map((subtask: any) => {
                        if (subtask.subtasks.length > 0 ) {
                            subtask.subtasks.map((child: any) => {
                                if (child.subtasks.length > 0 ) {
                                    child.subtasks.map((child1: any) => {
                                        if (child1.subtasks.length > 0) {
                                            child1.subtasks.map((children: any) => {
                                                children[newcolumnName] = this.columnForm.value.columndefaultvalue;
                                            });
                                        }
                                        child1[newcolumnName] = this.columnForm.value.columndefaultvalue;
                                    });
                                }
                                child[newcolumnName] = this.columnForm.value.columndefaultvalue;
                            });
                        }
                        subtask[newcolumnName] = this.columnForm.value.columndefaultvalue;
                    });
                }
                item[newcolumnName] = this.columnForm.value.columndefaultvalue;
                console.log(item);
            });
            console.log(this.columnForm.value.columndefaultvalue);
            let column : any = {
                field: this.columnForm.value.columnname,
                headerText: this.columnForm.value.columnname,
                value: this.columnForm.value.columndefaultvalue,
                type: this.columnForm.value.columntype,
                minWidth: this.columnForm.value.columnwidth,
                textAlign: this.columnForm.value.columnalignment,
                customAttributes: customStyle
            };
            column.style = customStyle;
            //(document.getElementsByClassName('e-headercell')[1] as any).style = "background-color: green";
            if (this.columnForm.value.columntype === 'boolean') {
                column.displayAsCheckBox = true;
            }
            if (this.columnForm.value.columntype === 'date') {
                column.edit = this.dpParams;
                column.format = this.format;
            }
            if (this.columnForm.value.columntype === 'datetime') {
                column.edit = this.dpParams;
                column.format = this.format;
            }
            this.treegrid.allowTextWrap = this.columnForm.value.columntextwrap;
            this.treegrid.columns.push(column);
            this.treegrid.refreshColumns();
            this.treegrid.refresh();
            this.template.hide();
        }
        if (this.columnMode === 'Edit') {
           console.log('edit column');
           console.log(this.selectedEditColumn);
            let customStyle: any = {
                color: this.columnForm.value.columnfontcolor,
                'background-color': this.columnForm.value.columnfontbgcolor
            };
            let editcolumnName = this.selectedEditColumn;
            sampleData.map((item: any) => {
                if (item.subtasks.length > 0 ) {
                    item.subtasks.map((subtask: any) => {
                        if (subtask.subtasks.length > 0 ) {
                            subtask.subtasks.map((child: any) => {
                                if (child.subtasks.length > 0 ) {
                                    child.subtasks.map((child1: any) => {
                                        if (child1.subtasks.length > 0) {
                                            child1.subtasks.map((children: any) => {
                                                delete children[editcolumnName];
                                                children[editcolumnName] = this.columnForm.value.columndefaultvalue;
                                            });
                                        }
                                        delete child1[editcolumnName];
                                        child1[editcolumnName] = this.columnForm.value.columndefaultvalue;
                                    });
                                }
                                delete child[editcolumnName];
                                child[editcolumnName] = this.columnForm.value.columndefaultvalue;
                            });
                        }
                        delete subtask[editcolumnName]
                        subtask[editcolumnName] = this.columnForm.value.columndefaultvalue;
                    });
                }
                delete item[editcolumnName];
                item[editcolumnName] = this.columnForm.value.columndefaultvalue;
                console.log(item);
            });
            console.log(this.columnForm.value.columndefaultvalue);
            let column : any = {
                field: this.columnForm.value.columnname,
                headerText: this.columnForm.value.columnname,
                value: this.columnForm.value.columndefaultvalue,
                type: this.columnForm.value.columntype,
                minWidth: this.columnForm.value.columnwidth,
                textAlign: this.columnForm.value.columnalignment,
                customAttributes: customStyle
            };
            column.style = customStyle;
            //(document.getElementsByClassName('e-headercell')[1] as any).style = "background-color: green";
            if (this.columnForm.value.columntype === 'boolean') {
                column.displayAsCheckBox = true;
            }
            if (this.columnForm.value.columntype === 'date') {
                column.edit = this.dpParams;
                column.format = this.format;
            }
            if (this.columnForm.value.columntype === 'datetime') {
                column.edit = this.dpParams;
                column.format = this.format;
            }
            this.treegrid.allowTextWrap = this.columnForm.value.columntextwrap;
            // this.treegrid.columns.push(column);
            this.treegrid.refreshColumns();
            this.treegrid.refresh();
            this.template.hide();
        }

    }
    // contextMenuOpen(args: BeforeOpenCloseEventArgs) {
    //     //console.log(args);
    //     console.log('open');
    //     let items: Array<HTMLElement> = [].slice.call(document.querySelectorAll('.e-contextmenu'));
    //     console.log(items);
    //     for (let i: number = 0; i < items.length; i++) {
    //         items[i].setAttribute('style','color: red;');
    //     }
    //     //this.rowIndex = args.rowIndex;
    //     //this.cellIndex = args.cellIndex;
    // }
    actionComplete(args: DialogEditEventArgs) {

        // if (args.requestType === "delete") {
        //     this.treegrid.editSettings.showDeleteConfirmDialog = true;
        // }
    }
    dialogOpen (event: any): void {
        // Call the show method to open the Dialog
        this.template.hide();
    }

    customContextMenuClick(args: MenuEventArgs): void {
        console.log("scope is" + args.item.id);
        if (args.item.id === 'addnext') {
            this.treegrid.editSettings.newRowPosition = 'Below';
            this.rowPosition = 'Below';
            this.treegrid.addRecord();
            console.log("scope is" + this.rowPosition);
        } else if (args.item.id === 'addchild') {
            this.treegrid.editSettings.newRowPosition = 'Child';
            this.rowPosition = 'Child';
            this.treegrid.addRecord();
            console.log("scope is" + this.rowPosition);
        } else if (args.item.id === 'delete') {
            console.log(args.item);
            this.treegrid.deleteRecord()
            //this.treegrid.editSettings.showDeleteConfirmDialog = true;
             //this.treegrid.editSettings.showDeleteConfirmDialog = true;
        } else if (args.item.id === 'editrow') {
            //this.treegrid.editSettings.mode = 'Dailog';
            this.treegrid.startEdit();
             //this.treegrid.editSettings.mode = 'Dailog';
        } else if (args.item.id === 'multiselect') {
            this.treegrid.selectionSettings.type = 'Multiple' ;
        } else if (args.item.id === 'copyrows') {
           this.treegrid.copy(true);
        } else if (args.item.id === 'cutrows') {

        } else if (args.item.id === 'pastenext') {
            let rowIndex = this.rowIndex;
            let cellIndex = this.cellIndex;
            (document.getElementsByClassName('e-headercell')[1] as any).style.cssText = "";
            //let copyContent = this.treegrid.clipboardModule.copyContent;
            //this.treegrid.paste(copyContent, rowIndex, cellIndex);
        } else if (args.item.id === 'pastchild') {
            let rowIndex = this.rowIndex;
            let cellIndex = this.cellIndex;
            (document.getElementsByClassName('e-headercell')[1] as any).style.cssText = "";
            //let copyContent = this.treegrid.clipboardModule.copyContent;
            //this.treegrid.paste(copyContent, rowIndex, cellIndex);
        }
    }

    actionBegin(args: SaveEventArgs) {
        console.log("scope is" + args.requestType);
        if (args.requestType === "beginEdit" || args.requestType === "add") {
            console.log('yes');
        }
    }
    create() {
        console.log('created');
        let items: Array<HTMLElement> = [].slice.call(document.querySelectorAll('.e-contextmenu-wrapper'));
        console.log(items);
        for (let i: number = 0; i < items.length; i++) {
            items[i].setAttribute('style','color: red;');
        }
    }

    beforeOpen(args: any): void {
        this.isEditCol = localStorage.getItem('editcol') == "true" ? true : false;
        console.log(args);
        let columns = this.treegrid.getColumns();
        for (let i = 0; i < columns.length; i++) {
            if (columns[i].headerText === args.event.target.innerText) {
                this.selectedEditColumn = columns[i].field;
                console.log(columns[i]);
                this.columnForm.setValue({
                    columnname: columns[i].headerText,
                    columntype: columns[i].type,
                    columndefaultvalue: '',
                    columnwidth: columns[i].width,
                    columnfontsize: '',
                    columnfontcolor: '#000000',
                    columnfontbgcolor: '#ffffff',
                    columnalignment: columns[i].textAlign,
                    columntextwrap: false,
                })
            }
        }

        //this.headercontextmenu.enableItems(['EditCol'], this.isEditCol);
        // let targetNodeId: string = this.treegrid.selectedNodes[0];
        // alert("In Before open event" + "\n" + "SelectedNode:" + targetNodeId);
    }

    select(args: MenuEventArgs) {
        if (args.item.id === 'editcol') {
            let editCol = localStorage.getItem('editcol') == "true" ? true : false;
            console.log('Selecct'+ editCol);
            this.columnMode = 'Edit';
            //this.template.show();
            console.log("scope is" + args.item);
            //const column = this.treegrid.getHeaderContent();
            // const column = this.treegrid.getColumns();
            // this.domResult = document.getElementsByClassName("e-columnheader");
            // let result = this.domResult[0].childNodes;
            // console.log(result);
            // for (let i = 0; i < result.length; i++) {
            //     if (this.domResult[0].childNodes[i].tabIndex === 0) {
            //         console.log(result[i]);
            //         console.log(this.domResult[0].childNodes[i].innerText);
            //         const column = this.treegrid.getColumnByField(this.domResult[0].childNodes[i].innerText.toString());
            //         console.log(column);
            //     }
            // }
            //let result = this.columngrid.nativeElement.children;
            // column.headerText = 'Changed Text'; // assign a new header text to the column
            // this.treegrid.refreshColumns();
        } else if (args.item.id === 'newcol') {
            this.columnMode = 'Add';
            this.createForm();
            this.template.show();
            // let column : any = { field: 'taskDD', headerText: 'ggg', width: 50};
            // this.treegrid.columns.push(column);
            // this.treegrid.refreshColumns();
            // let targetNodeId: string = this.treegrid.selectedNodes[0];
            // let index: number = 1;
            // let nodeId: string = "tree_" + index;
            // let item: { [key: string]: Object } = { id: nodeId, name: "New Folder" };
            // this.treegrid.addNodes([item], targetNodeId, null);
            // index++;
            // sampleData.push(item);
            // this.treegrid.beginEdit(nodeId);
        } else if (args.item.id === 'delcol') {
            console.log("scope is" + args.item.id);
            this.columnMode = 'Delete';
            this.DeleteDialog.show();
        } else if (args.item.id === 'choosecol') {
            console.log("scope is" + args.item.id);
            this.treegrid.columnChooserModule.openColumnChooser();
        } else if (args.item.id === 'freezecol') {
            //console.log("column index: " + this.treegrid.columns.indexOf);
            //this.treegrid.columns.indexOf;
            this.treegrid.frozenColumns = 1;
            //this.treegrid.refreshColumns();
            this.treegrid.refreshHeader();
        } else if (args.item.id === 'filtercol') {
            console.log("scope is" + args.item.id);
            if(!this.treegrid.allowFiltering) {
                this.treegrid.allowFiltering = true;
            }
        } else if (args.item.id === 'multisort') {
            this.treegrid.allowSorting = true;
        }
    }
    selectRowContext(args: MenuEventArgs) {
        console.log("scope is" + args.item.id);
        if (args.item.id === 'addnext') {
            this.treegrid.editSettings.newRowPosition = 'Below';
            this.rowPosition = 'Below';
            this.treegrid.addRecord();
            console.log("scope is" + this.rowPosition);
        } else if (args.item.id === 'addchild') {
            this.treegrid.editSettings.newRowPosition = 'Child';
            this.rowPosition = 'Child';
            this.treegrid.addRecord();
            console.log("scope is" + this.rowPosition);
        } else if (args.item.id === 'delete') {
            console.log(args.item);
            this.treegrid.deleteRecord()
            //this.treegrid.editSettings.showDeleteConfirmDialog = true;
            //this.treegrid.editSettings.showDeleteConfirmDialog = true;
        } else if (args.item.id === 'editrow') {
            //this.treegrid.editSettings.mode = 'Dailog';
            this.treegrid.startEdit();
            //this.treegrid.editSettings.mode = 'Dailog';
        } else if (args.item.id === 'multiselect') {
            this.treegrid.selectionSettings.type = 'Multiple' ;
        } else if (args.item.id === 'copyrows') {
            this.treegrid.copy(true);
        } else if (args.item.id === 'cutrows') {

        } else if (args.item.id === 'pastenext') {
            let rowIndex = this.rowIndex;
            let cellIndex = this.cellIndex;
            (document.getElementsByClassName('e-headercell')[1] as any).style.cssText = "";
            //let copyContent = this.treegrid.clipboardModule.copyContent;
            //this.treegrid.paste(copyContent, rowIndex, cellIndex);
        } else if (args.item.id === 'pastchild') {
            let rowIndex = this.rowIndex;
            let cellIndex = this.cellIndex;
            (document.getElementsByClassName('e-headercell')[1] as any).style.cssText = "";
            //let copyContent = this.treegrid.clipboardModule.copyContent;
            //this.treegrid.paste(copyContent, rowIndex, cellIndex);
        }
    }
    cancel() {
        this.template.hide();
    }

    dialogdeleteClose(){
    }
    // On Dialog open, hide the buttons
    dialogdeleteOpen() {
        //this.DeleteDialog.hide();
    }
    deleteColumn() {
        if(this.columnMode === 'Delete') {
            let editcolumnName = this.selectedEditColumn;
            sampleData.map((item: any) => {
                if (item.subtasks.length > 0 ) {
                    item.subtasks.map((subtask: any) => {
                        if (subtask.subtasks.length > 0 ) {
                            subtask.subtasks.map((child: any) => {
                                if (child.subtasks.length > 0 ) {
                                    child.subtasks.map((child1: any) => {
                                        if (child1.subtasks.length > 0) {
                                            child1.subtasks.map((children: any) => {
                                                delete children[editcolumnName];
                                            });
                                        }
                                        delete child1[editcolumnName];
                                    });
                                }
                                delete child[editcolumnName];
                            });
                        }
                        delete subtask[editcolumnName]
                    });
                }
                delete item[editcolumnName];
                console.log(item);
            });
            const column = this.treegrid.getColumns();
            console.log(column);
            this.columnsData.filter((i:any,x: any) => {
                if(i.field == editcolumnName) {
                    this.columnsData.splice(x,1);
                }
            });
            this.treegrid.refreshColumns();
            this.treegrid.refresh();
            this.DeleteDialog.hide();
        }
    }
    cancelColumn() {
        this.DeleteDialog.hide();
    }

}
