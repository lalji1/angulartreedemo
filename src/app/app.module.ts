import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropDownListModule } from '@syncfusion/ej2-angular-dropdowns';
import { GridModule } from '@syncfusion/ej2-angular-grids';
import { TreeGridAllModule } from '@syncfusion/ej2-angular-treegrid';
import { DialogModule } from '@syncfusion/ej2-angular-popups';
import { DatePickerModule, DateTimePickerModule, DatePickerAllModule } from '@syncfusion/ej2-angular-calendars';
import { ColorPickerModule  } from '@syncfusion/ej2-angular-inputs';
import { ContextMenuModule  } from '@syncfusion/ej2-angular-navigations';
import { CheckBoxModule, ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    DropDownListModule,
    GridModule,
    TreeGridAllModule,
    DialogModule,
    DatePickerModule,
    DateTimePickerModule,
    ColorPickerModule,
    ContextMenuModule,
    CheckBoxModule,
    ButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
